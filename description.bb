[img=https://i.imgur.com/q7WjIs1.png][/img]
                                                      [b][url=https://gitlab.com/dagriefaa/additive-loading]GitLab Repository[/url][/b]

[h1][b]                                  Enable Advanced Building.[/b][/h1]
                                     And check the mod is enabled in the mod menu.

[img]https://i.imgur.com/DVn6bSA.gif[/img]

This mod lets you merge new machines and levels into the current one, and lets you save (machine) selections.
[b]This will not rebind your keys for you![/b]

Workshop machines/levels are not supported - save them to your computer first.

Use at your own risk; if your computer melts down because you decided to merge two 6000-object levels, you probably shouldn't have tried to merge two 6000-object levels.

[h1]Extra Features[/h1]
[list]
[*]It will make the save/load menu remember which folder you're in when you close it.
[*]If you have [url=https://steamcommunity.com/sharedfiles/filedetails/?id=2065953011]Block Toggle Utility[/url] installed, you can import more than one starting block at a time.
[/list]