﻿using UnityEngine;

namespace AdditiveLoading {
    public class UIScaleOnMouseOver : MonoBehaviour {

        public void Start() {
            startScale = transform.localScale;
            pressedScale = (startScale * PressedScale);
            overScale = (startScale * HoverScale);
        }

        private void OnMouseDown() {
            if (!this.enabled || !UIMask.InsideMask(mask, this.transform.position) || !active || !isMouseOver) {
                return;
            }
            ScaleObject(pressedScale);
        }

        private void OnMouseUp() {
            if (!this.enabled || !UIMask.InsideMask(mask, this.transform.position)) {
                OnMouseExit();
                return;
            }
            if (!active || !isMouseOver) {
                return;
            }
            ScaleObject(overScale);
        }

        private void OnMouseEnter() {
            if (!this.enabled || !UIMask.InsideMask(mask, this.transform.position)) {
                isMouseOver = false;
                return;
            }
            if (!active) {
                return;
            }
            isMouseOver = true;
            ScaleObject(overScale);
        }

        private void OnMouseExit() {
            if (!active) {
                return;
            }
            isMouseOver = false;
            ScaleObject(startScale);
        }

        private void OnDisable() {
            isMouseOver = false;
            if (startScale != default) {
                this.transform.localScale = startScale;
            }
        }

        private void ScaleObject(Vector3 newScale) {
            this.transform.localScale = newScale;
        }

        private void SetEnabledMsg(bool enabled) {
            OnMouseExit();
            active = enabled;
        }

        public float HoverScale = 1.2f;
        public float PressedScale = 0.85f;

        public float lerpSpeed = 0.1f;

        private Vector3 startScale;
        private Vector3 overScale;
        private Vector3 pressedScale;

        public int mask = -1;
        private bool isMouseOver;
        private bool active = true;
    }
}
