﻿using Modding;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AdditiveLoading {
    class AdditiveLoadingController : MonoBehaviour {

        FileBrowserView browser;
        GameObject mergeParent;

        void Start() {
            Events.OnActiveSceneChanged += OnSceneChanged;
            if (ModResource.AllResourcesLoaded) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }
            else {
                ModResource.OnAllResourcesLoaded += () => OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }
        }

        void OnSceneChanged(Scene p, Scene c) {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            MergeButton.buttonTex = ModResource.GetTexture("icon").Texture;

            browser = GameObject.Find("HUD").transform.FindChild("FileBrowserView").GetComponent<FileBrowserView>();

            GameObject loadSaveParent = GameObject.Find("HUD").transform.FindChild("FileBrowserView/TopBar/ObjectTextField/ButtonBG").gameObject;
            mergeParent = (GameObject)Instantiate(loadSaveParent, loadSaveParent.transform.position, loadSaveParent.transform.rotation);

            GameObject loadSaveButton = loadSaveParent.transform.FindChild("LoadSaveButton").gameObject;
            loadSaveButton.transform.localScale *= 1.1f;
            loadSaveButton.AddComponent<UIScaleOnMouseOver>();

            mergeParent.transform.SetParent(loadSaveParent.transform.parent);
            mergeParent.transform.localPosition = new Vector3(2.05f, 0.01f, 0.15f);

            GameObject mergeButton = mergeParent.transform.FindChild("LoadSaveButton").gameObject;
            mergeButton.name = "MergeButton";
            mergeButton.transform.localScale = Vector3.one * 0.4f;
            mergeButton.transform.localPosition = new Vector3(0, 0, -0.5f);
            mergeButton.AddComponent<UIScaleOnMouseOver>();

            List<OpenWorkshopButton> workshopButtons = Resources.FindObjectsOfTypeAll<OpenWorkshopButton>()?.ToList();
            workshopButtons.ForEach(w => { w.transform.position = mergeButton.transform.position + Vector3.right * 0.76f; });
            DestroyImmediate(mergeButton.GetComponent<LoadSaveButton>());
            mergeButton.AddComponent<MergeButton>();
        }

        void Update() {
            if (Mod.SceneNotPlayable()) {
                return;
            }

            mergeParent?.SetActive(
                !(browser.IsSaveMenu && (browser.Controller is LevelFileBrowserController || AdvancedBlockEditor.Instance.selectionController.Selection.Count == 0))
                && !FileBrowserView.saveMenuUpload
            );
        }
    }
}
