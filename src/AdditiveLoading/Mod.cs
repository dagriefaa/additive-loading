using Modding;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AdditiveLoading {
    public class Mod : ModEntryPoint {

        public override void OnLoad() {
            GameObject controller = GameObject.Find("ModControllerObject");
            if (!controller) { Object.DontDestroyOnLoad(controller = new GameObject("ModControllerObject")); }
            controller.AddComponent<AdditiveLoadingController>();
        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }
    }
}
