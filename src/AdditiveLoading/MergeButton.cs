﻿using Modding;
using Modding.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AdditiveLoading {
    class MergeButton : SimpleUIButton {

        class FileMaker : LocalMachineCollection {
            public override CreateFileResult CreateFile(string fileName, out VirtualFile virtualObject) {
                return base.CreateFile(fileName, out virtualObject);
            }
        }

        static readonly Guid[] startingBlockHandlingMods = {
            new Guid("84afd81f-1d07-4e70-99ec-4e8107f60062")
        };

        FileBrowserView browser;
        UITextField nameText;
        TextMesh fullPathText;
        BoxCollider confirmBox;

        MeshRenderer buttonRenderer;

        public static Texture buttonTex;

        bool overwriteConfirm = false;

        public void Start() {
            this.Click += OnClick;

            browser = GameObject.Find("HUD").transform.FindChild("FileBrowserView").GetComponent<FileBrowserView>();
            nameText = GameObject.Find("HUD").transform.FindChild("FileBrowserView/TopBar/ObjectTextField/Object Text Field").GetComponent<UITextField>();
            fullPathText = GameObject.Find("HUD").transform.FindChild("FileBrowserView/TopBar/FolderTextMesh").GetComponent<TextMesh>();

            confirmBox = GameObject.Find("HUD").transform.FindChild("FileBrowserView/TopBar/OverwriteButton").GetComponent<BoxCollider>();
            foreach (Transform t in confirmBox.transform) {
                t.localPosition += Vector3.right * 4;
            }

            buttonRenderer = this.gameObject.GetComponent<MeshRenderer>();
            buttonRenderer.material.mainTexture = buttonTex;
        }

        void Update() {
            buttonRenderer.material.SetColor("_TintColor",
                (AdvancedBlockEditor.Instance.selectionController.Selection.Count > 0 && browser.IsSaveMenu)
                    ? new Color(2f, 0.4f, 0f, 0.5f)
                    : new Color(1, 1, 1, 0.5f));
        }

        public void OnClick() {
            if (nameText.Text == "") {
                return;
            }

            IVirtualObject virtualObject = browser.Controller.FindVirtualObject(nameText.Text);
            if (virtualObject != null && virtualObject is VirtualFolder) {
                return;
            }

            if (browser.Controller is MachineFileBrowserController) {
                if (browser.IsSaveMenu) {
                    if (virtualObject == null) {
                        string[] completePath = fullPathText.text.Split(new[] { "/SavedMachines" }, StringSplitOptions.None);
                        using (FileMaker fm = new FileMaker()) {
                            fm.ChangeFolder(fm.GetRoot());
                            if (completePath.LastOrDefault() != "/") {
                                string[] relativePath = completePath.LastOrDefault().Split('/');
                                foreach (string folder in relativePath) {
                                    if (folder == "") {
                                        continue;
                                    }
                                    IVirtualObject next = fm.CurrentFolder.GetObjects().FirstOrDefault(x => x.Name == folder && x.IsFolder);
                                    fm.ChangeFolder(next as VirtualFolder);
                                }
                            }
                            fm.CreateFile(nameText.Text, out VirtualFile file);
                            virtualObject = file;
                            if (virtualObject == null) {
                                Debug.LogError("Failed to create selection save file", this);
                                return;
                            }
                        }
                    }
                    else if (!overwriteConfirm || !confirmBox.enabled) {
                        browser.ToggleOverwriteButton(false);
                        browser.ToggleOverwriteButton(true);
                        overwriteConfirm = true;
                        return;
                    }
                    SaveSelection(virtualObject);
                    browser.ToggleOverwriteButton(false);
                    overwriteConfirm = false;
                }
                else {
                    ImportMachine(virtualObject);
                    FlushNameField("SavedMachines");
                }
            }
            else if (browser.Controller is LevelFileBrowserController) {
                ImportLevel(virtualObject);
                FlushNameField("CustomLevels");
            }
        }

        void FlushNameField(string split) {
            string currentPath = fullPathText.text.Split(new[] { split }, StringSplitOptions.None).LastOrDefault();

            foreach (string s in FileBrowserView.FieldValues.Keys) {
                string partialPath = s.Split(new[] { split }, StringSplitOptions.None).LastOrDefault();
                if (currentPath == partialPath) {
                    FileBrowserView.FieldValues.Remove(s);
                    break;
                }
            }
        }

        public void ImportMachine(IVirtualObject virtualObject) {
            if (virtualObject == null) {
                return;
            }

            // setup
            Machine.Active().isLoadingInfo = true;
            if (StatMaster.isMP) {
                StatMaster.cachingTransformActions = true;
            }
            AdvancedBlockEditor.Instance.SetActiveTool(StatMaster.Tool.Translate);
            AdvancedBlockEditor.Instance.selectionController.DeselectAll(false, true);
            bool mergeState = StatMaster.mergeSurfaceTypesOnDeselect;
            StatMaster.mergeSurfaceTypesOnDeselect = false;

            // load machine info
            MachineInfo info;
            try {
                info = XmlLoader.LoadFromFullPath(virtualObject.ObjectPath.Path);
                Debug.Log($"Importing machine in XML format: {nameText.Text}.bsg", this);
            }
            catch { // outdated format
                info = MachineFormatConverter.ConvertBsgToMachineInfo(nameText.Text, virtualObject.ObjectPath.Path);
                Debug.Log($"Importing machine in old format: {nameText.Text}.bsg", this);
            }

            bool requiredDlcInstalled = XmlLoader.ExternalDLCCheck(info);
            if (!requiredDlcInstalled) {
                GenericUIPopup.Instance.Show("This machine requires DLC that you do not own.", 5);
                return;
            }

            List<BlockBehaviour> blocksToSelect = new List<BlockBehaviour>();
            List<UndoAction> undoActions = new List<UndoAction>();
            Dictionary<Guid, BlockBehaviour> guidMap = new Dictionary<Guid, BlockBehaviour>();
            bool hasStartingBlock = Machine.Active().GetBlocks(BlockType.StartingBlock).Count > 0;

            foreach (BlockInfo blockInfo in info.Blocks) {

                Guid oldGuid = blockInfo.Guid;
                blockInfo.Guid = Guid.NewGuid();
                bool isStartingBlock = false;

                if (blockInfo.ID == BlockType.StartingBlock && !startingBlockHandlingMods.Any(Mods.IsModLoaded)) {

                    if (!hasStartingBlock) {
                        hasStartingBlock = true;
                    }
                    else {
                        blockInfo.ID = BlockType.Ballast;
                        isStartingBlock = true;
                        blockInfo.BlockData.Write("bmt-mass", 0.25f);
                    }

                }
                else if (blockInfo.ID == BlockType.BuildEdge) {
                    if (!blockInfo.BlockData.HasKey("start") || !blockInfo.BlockData.HasKey("end")) {
                        Debug.Log($"BuildEdge {oldGuid} has no start/end node!", this);
                        continue;
                    }
                    if (!guidMap.TryGetValue(new Guid(blockInfo.BlockData.ReadString("start")), out BlockBehaviour startNode)) {
                        Debug.LogError($"BuildEdge {oldGuid} cannot find start node {blockInfo.BlockData.ReadString("start")}!", this);
                        continue;
                    }
                    if (!guidMap.TryGetValue(new Guid(blockInfo.BlockData.ReadString("end")), out BlockBehaviour endNode)) {
                        Debug.LogError($"BuildEdge {oldGuid} cannot find end node {blockInfo.BlockData.ReadString("end")}!", this);
                        continue;
                    }
                    BuildEdgeBlock.WriteData(blockInfo.BlockData, startNode as BuildNodeBlock, endNode as BuildNodeBlock);
                }
                else if (blockInfo.ID == BlockType.BuildSurface) {
                    if (!blockInfo.BlockData.HasKey("edges")) {
                        Debug.LogError($"BuildSurface {oldGuid} has no edges!", this);
                        continue;
                    }

                    string[] edgeStrings = blockInfo.BlockData.ReadString("edges").Split('|');
                    BuildEdgeBlock[] edges = new BuildEdgeBlock[edgeStrings.Length];
                    bool nullEdges = false;

                    for (int k = 0; k < edgeStrings.Length; k++) {
                        if (guidMap.TryGetValue(new Guid(edgeStrings[k]), out BlockBehaviour newEdge)) {
                            edges[k] = (newEdge as BuildEdgeBlock);
                        }
                        else {
                            Debug.LogError($"Couldn't find surface edge {k}!", this);
                            nullEdges = true;
                            break;
                        }
                    }
                    if (nullEdges) { continue; }

                    BuildSurface.WriteData(blockInfo.BlockData, edges);
                }

                if (Machine.Active().AddBlock(blockInfo, out BlockBehaviour newBlock)) {

                    blockInfo.Guid = newBlock.Guid;
                    if (isStartingBlock) {
                        newBlock.SetPosition(newBlock.transform.position - newBlock.transform.forward * 0.5f * newBlock.transform.localScale.z);
                    }

                    newBlock.VisualController.PlaceFromBlockInfo(blockInfo);
                    blocksToSelect.Add(newBlock);
                    if (blockInfo.ID == BlockType.BuildNode || blockInfo.ID == BlockType.BuildEdge) {
                        guidMap.Add(oldGuid, newBlock);
                    }
                    undoActions.Add(new UndoActionAdd(Machine.Active(), blockInfo));
                }
            }

            Machine.Active().isLoadingInfo = false;
            if (StatMaster.isMP) {
                PlayerMachine.GetLocal().InternalObjectServer.DetermineBannedBlocks();
                PlayerMachine.GetLocal().InternalObjectServer.FlushBlockTransformActions();
            }

            if (undoActions.Count > 0) {
                AdvancedBlockEditor.Instance.selectionController.Select(blocksToSelect, false, false);
                Machine.Active().UndoSystem.AddActions(undoActions);

                SingleInstanceFindOnly<AddPiece>.Instance.SingleHammerAnimate(
                    AdvancedBlockEditor.Instance.selectionController.LastBlock.transform.position,
                    AdvancedBlockEditor.Instance.selectionController.LastBlock.transform.position,
                    AdvancedBlockEditor.Instance.selectionController.LastBlock.transform.forward
                );

                Machine.Active().onBatchOperationComplete();
            }

            browser.Close();
            StatMaster.mergeSurfaceTypesOnDeselect = mergeState;
            SingleInstanceFindOnly<AddPiece>.Instance.UpdateMiddleOfObject(true);
        }

        public void SaveSelection(IVirtualObject virtualObject) {
            if (AdvancedBlockEditor.Instance.selectionController.Selection.Count == 0) {
                browser.Close();
                return;
            }
            try {

                MachineInfo machineInfo = new MachineInfo {
                    Name = nameText.Text,
                    Position = Machine.Active().Position,
                    Rotation = Machine.Active().Rotation
                };

                // sort nodes and edges to the top of the list to ensure they load properly
                List<BlockInfo> others = new List<BlockInfo>();
                foreach (BlockInfo b in AdvancedBlockEditor.Instance.selectionController.MachineSelection.ConvertAll(BlockInfo.FromBlockBehaviour)) {
                    switch (b.ID) {

                        case BlockType.BuildNode:
                            machineInfo.Blocks.Insert(0, b);
                            break;

                        case BlockType.BuildEdge:
                            machineInfo.Blocks.Add(b);
                            break;

                        default:
                            others.Add(b);
                            break;
                    }
                }

                machineInfo.Blocks.AddRange(others);

                if (others.Count == 0) {
                    Debug.LogWarning("Selection only contains surface nodes or edges. Save aborted.", this);
                    return;
                }

                // make thumbnail
                string s = virtualObject.ObjectPath.Path;
                int lastSlashIndex = s.LastIndexOf('/');
                string directory = s.Substring(0, lastSlashIndex);
                string file = s.Substring(lastSlashIndex + 1);
                file = file.Substring(0, file.LastIndexOf('.'));
                string thumbnailPath = $"{directory}/Thumbnails/{file}.png";
                browser.CreateThumbnail(thumbnailPath, false);

                XmlSaver.Save(machineInfo, directory);

                browser.Close();

            }
            catch (Exception e) {
                Debug.LogException(e);
            }
        }

        public void ImportLevel(IVirtualObject virtualObject) {
            if (virtualObject == null) return;

            LevelSettings oldSettings = LevelEditor.Instance.Settings;
            List<EntityController.PlaceEntry> list = new List<EntityController.PlaceEntry>();
            foreach (LevelEntity levelEntity in LevelEditor.Instance.Entities) { // cache
                if (!(levelEntity == null)) {
                    list.Add(new EntityController.PlaceEntry(levelEntity.behaviour.prefab.ID, levelEntity.Position, levelEntity.Rotation, levelEntity.Scale,
                        levelEntity.GetEntityData(), (long)UnityEngine.Random.value));
                }
            }

            browser.Controller.OpenObject(virtualObject, false); // -_-
            LevelEditor.Instance.UpdateLevelSettings(oldSettings);

            LevelEditor.Instance.SetActiveTool(StatMaster.Tool.Translate);
            var newCount = LevelEditor.Instance.Entities.Count;

            if (list.Count > 0) {
                FindObjectOfType<EntityController>().Add(list, false, false, true); // dump cache
            }

            LevelEditor.Instance.selectionController.DeselectAll(false);
            LevelEditor.Instance.selectionController.Select(LevelEditor.Instance.Entities.GetRange(0, newCount),
                true, false);

        }
    }
}
