<div align="center">
<img src="header.png" alt="Additive Loading" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1477701362<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1477701362?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1477701362?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1477701362?color=%231b2838&logo=steam&style=for-the-badge)
</div>

Additive Loading is a mod for [Besiege](https://store.steampowered.com/app/346010) which allows loading machines on top of the current machine (instead of deleting it outright), and saving only selections.

While this is used heavily for just loading more than one machine at once, it is also invaluable as a subassembly management tool. Parts of machines can be saved and reused easily, saving time and effort.

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
Usage instructions can also be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=1477701362).

In short:
* It adds a second 'save/load' button next to the address bar in the file menu.
* If loading, this button will load the machine/level without destroying the current machine.
* If saving, this button will save only the selected blocks.

It does not work for workshop machines; these need to be saved locally.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.
